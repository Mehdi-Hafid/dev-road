package com.derbyware.devroad.api;

import com.derbyware.devroad.dto.ProjectDTO;
import com.derbyware.devroad.logic.ProjectService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/api/project")
public class ProjectController {

	private final ProjectService projectService;

	@PostMapping
	public ResponseEntity<ProjectDTO> createVersion(@RequestBody ProjectDTO projectDTO) {
		ProjectDTO result = projectService.createProject(projectDTO);
		return new ResponseEntity<ProjectDTO>(result, HttpStatus.OK);
	}

	@GetMapping(value = "/{projectId}")
	public ResponseEntity<ProjectDTO> getVersion(@PathVariable("projectId") long projectId) {
		ProjectDTO projectDTO = projectService.get(projectId);
		return new ResponseEntity<ProjectDTO>(projectDTO, HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<List<ProjectDTO>> recent(@RequestParam(required = false, defaultValue = "0") int page) {
		List<ProjectDTO> recentTechnologies = projectService.recent(page);
		return new ResponseEntity<List<ProjectDTO>>(recentTechnologies, HttpStatus.OK);
	}

	@PutMapping("/{projectId}")
	public ResponseEntity<ProjectDTO> replace(@PathVariable("projectId") long projectId,
	                                          @RequestBody ProjectDTO projectDTO) {
		ProjectDTO updatedProjectDTO = projectService.replace(projectId, projectDTO);
		return new ResponseEntity<ProjectDTO>(updatedProjectDTO, HttpStatus.OK);
	}

	@DeleteMapping("/{projectId}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("projectId") long projectId) {
		projectService.delete(projectId);
	}
}
