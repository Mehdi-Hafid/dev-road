package com.derbyware.devroad.api;

import com.derbyware.devroad.dto.TechnologyDTO;
import com.derbyware.devroad.dto.VersionDTO;
import com.derbyware.devroad.logic.VersionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/api/version")
public class VersionController {

	private final VersionService versionService;

	/*  parent is ID of technology */
	@PostMapping(value = "/{technologyId}")
	public ResponseEntity<VersionDTO> createVersion(@PathVariable(value = "technologyId") long technologyId,
	                                                @RequestBody VersionDTO versionDTO) {
		VersionDTO result = versionService.createVersion(technologyId, versionDTO);
		return new ResponseEntity<VersionDTO>(result, HttpStatus.OK);
	}

	@GetMapping(value = "/{technologyId}/recent")
	public ResponseEntity<List<VersionDTO>> recent(@PathVariable(value = "technologyId") long technologyId,
	                                               @RequestParam(required = false, defaultValue = "0") int page) {
		List<VersionDTO> recentVersions = versionService.recent(technologyId, page);
		return new ResponseEntity<List<VersionDTO>>(recentVersions, HttpStatus.OK);
	}

	@GetMapping(value = "/{versionId}")
	public ResponseEntity<VersionDTO> getVersion(@PathVariable("versionId") long versionId) {
		VersionDTO t = versionService.get(versionId);
		return new ResponseEntity<VersionDTO>(t, HttpStatus.OK);
	}

	@PatchMapping("/{versionId}")
	public ResponseEntity<VersionDTO> updatePartially(@PathVariable("versionId") long versionId,
	                                                  @RequestBody VersionDTO updateVersionDTO) {
		VersionDTO updatedTechnology = versionService.update(versionId, updateVersionDTO);
		return new ResponseEntity<VersionDTO>(updatedTechnology, HttpStatus.OK);
	}

	@DeleteMapping("/{versionId}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("versionId") long versionId) {
		versionService.delete(versionId);
	}

	@PatchMapping("/null/{versionId}")
	public ResponseEntity<VersionDTO> nullify(@PathVariable("versionId") long versionId,
	                                             @RequestBody Map<String, Object> updates){
		VersionDTO versionDTO = versionService.nullify(versionId, updates);
		return new ResponseEntity<VersionDTO>(versionDTO, HttpStatus.OK);
	}

}