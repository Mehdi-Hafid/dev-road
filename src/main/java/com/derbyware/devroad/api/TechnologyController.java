package com.derbyware.devroad.api;

import com.derbyware.devroad.dto.TechnologyDTO;
import com.derbyware.devroad.logic.TechnologyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/api/tech")
public class TechnologyController {

	private final TechnologyService technologyService;

	/* {name, imageLink, parent}
	 * parent is ID of technology */
	@PostMapping
	public ResponseEntity<TechnologyDTO> createTechnology(@RequestBody TechnologyDTO technologyDTO) {
		TechnologyDTO result = technologyService.createTechnology(technologyDTO);
		return new ResponseEntity<TechnologyDTO>(result, HttpStatus.OK);
	}

	@GetMapping(value = "/recent")
	public ResponseEntity<List<TechnologyDTO>> recent(
			@RequestParam(required = false, defaultValue = "0") int page) {
		List<TechnologyDTO> recentTechnologies = technologyService.recent(page);
		return new ResponseEntity<List<TechnologyDTO>>(recentTechnologies, HttpStatus.OK);
	}

	@GetMapping(value = "/{technologyId}")
	public ResponseEntity<TechnologyDTO> getTechnology(@PathVariable("technologyId") long technologyId) {
		TechnologyDTO t = technologyService.get(technologyId);
		return new ResponseEntity<TechnologyDTO>(t, HttpStatus.OK);
	}

	@GetMapping(value = "/seoname/{seoname}")
	public ResponseEntity<TechnologyDTO> getTechnologyBySEOName(@PathVariable("seoname") String seoname) {
		TechnologyDTO t = technologyService.get(seoname);
		return new ResponseEntity<TechnologyDTO>(t, HttpStatus.OK);
	}

	@PatchMapping("/{technologyId}")
	public ResponseEntity<TechnologyDTO> updatePartially(@PathVariable("technologyId") long technologyId,
	                                                     @RequestBody TechnologyDTO updateTechnologyDTO) {
		TechnologyDTO updateTechnology = technologyService.update(technologyId, updateTechnologyDTO);
		return new ResponseEntity<TechnologyDTO>(updateTechnology, HttpStatus.OK);
	}

	// Do not remove
//	@PutMapping("/{technologyId}")
//	public ResponseEntity<TechnologyDTO> replace(@PathVariable("technologyId") long technologyId,
//	                                             @RequestBody TechnologyDTO updateTechnologyDTO) {
//		TechnologyDTO updateTechnology = technologyService.replace(technologyId, updateTechnologyDTO);
//		return new ResponseEntity<TechnologyDTO>(updateTechnology, HttpStatus.OK);
//	}

	@DeleteMapping("/{technologyId}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("technologyId") long technologyId) {
		technologyService.delete(technologyId);
	}

	@PatchMapping("/null/{technologyId}")
	public ResponseEntity<TechnologyDTO> nullify(@PathVariable("technologyId") long technologyId,
	                    @RequestBody Map<String, Object> updates){
		TechnologyDTO technologyDTO = technologyService.nullify(technologyId, updates);
		return new ResponseEntity<TechnologyDTO>(technologyDTO, HttpStatus.OK);
	}

}