package com.derbyware.devroad.api;

import com.derbyware.devroad.dto.UpgradeDTO;
import com.derbyware.devroad.entities.constructor.DisplayUpgrade;
import com.derbyware.devroad.logic.UpgradeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/api/upgrade")
public class UpgradeController {

	private final UpgradeService upgradeService;

	// api: versionId required, front: not required in url
	@PostMapping(value = "/{versionId}")
	public ResponseEntity<UpgradeDTO> createUpgrade(@PathVariable(value = "versionId") long versionId,
	                                                @RequestBody UpgradeDTO upgradeDTO) {
		UpgradeDTO result = upgradeService.createUpgrade(versionId, upgradeDTO);
		return new ResponseEntity<UpgradeDTO>(result, HttpStatus.OK);
	}

	@GetMapping(value = "/{upgradeId}")
	public ResponseEntity<UpgradeDTO> getUpgrade(@PathVariable("upgradeId") long upgradeId) {
		UpgradeDTO upgradeDTO = upgradeService.get(upgradeId);
		return new ResponseEntity<UpgradeDTO>(upgradeDTO, HttpStatus.OK);
	}

	@GetMapping(value = "/technology/{technologyId}")
	public ResponseEntity<DisplayUpgrade> recent(@PathVariable(value = "technologyId") long technologyId) {
		DisplayUpgrade displayUpgrade = upgradeService.getAll(technologyId);
		return new ResponseEntity<DisplayUpgrade>(displayUpgrade, HttpStatus.OK);
	}

	@GetMapping(value = "/technology/seoname/{seoname}")
	public ResponseEntity<DisplayUpgrade> recentDisplayUpgradeTechnology(@PathVariable(value = "seoname") String seoname) {
		DisplayUpgrade displayUpgrade = upgradeService.getAll(seoname);
		return new ResponseEntity<DisplayUpgrade>(displayUpgrade, HttpStatus.OK);
	}

	// Must always get old values in the request
	@PatchMapping("/{upgradeId}")
	public ResponseEntity<UpgradeDTO> updatePartially(@PathVariable("upgradeId") long upgradeId,
	                                                  @RequestBody UpgradeDTO upgradeDTO) {
		UpgradeDTO updatedUpgradeDTO = upgradeService.update(upgradeId, upgradeDTO);
		return new ResponseEntity<UpgradeDTO>(updatedUpgradeDTO, HttpStatus.OK);
	}
	// add replace too.

//	@PutMapping("/{upgradeId}")
//	public ResponseEntity<UpgradeDTO> replace(@PathVariable("upgradeId") long upgradeId,
//	                                             @RequestBody UpgradeDTO upgradeDTO) {
//		UpgradeDTO updatedUpgradeDTO = upgradeService.replace(upgradeId, upgradeDTO);
//		return new ResponseEntity<UpgradeDTO>(updatedUpgradeDTO, HttpStatus.OK);
//	}

	@DeleteMapping("/{upgradeId}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("upgradeId") long upgradeId) {
		upgradeService.delete(upgradeId);
	}


}
