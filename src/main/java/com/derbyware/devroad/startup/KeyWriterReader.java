package com.derbyware.devroad.startup;

import com.derbyware.devroad.entities.KeyHolder;
import com.derbyware.devroad.repositories.KeyHolderRepository;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.io.Encoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@RequiredArgsConstructor
public class KeyWriterReader implements ApplicationRunner {

	private final KeyHolderRepository keyHolderRepository;

	public static java.security.Key key;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		System.out.println("Running KeyWriterReader" );
		List<KeyHolder> all = keyHolderRepository.findAll();
		KeyHolder keyHolder;
		if (all.isEmpty()) {
			keyHolder = keyHolderRepository.save(createKey());
		} else {
			keyHolder = all.get(0);
		}
		buildSecurityKey(keyHolder);
		System.out.println("End KeyWriterReader key" + Arrays.toString(key.getEncoded()));
	}

	private KeyHolder createKey() {
		java.security.Key key = Keys.secretKeyFor(SignatureAlgorithm.HS512);
		String encoded = Encoders.BASE64.encode(key.getEncoded());
		KeyHolder keyHolderEntity = new KeyHolder();
		keyHolderEntity.setEncoded(encoded);
		return keyHolderEntity;
	}

	private void buildSecurityKey(KeyHolder keyHolderEntity) {
		key = Keys.hmacShaKeyFor(Decoders.BASE64.decode(keyHolderEntity.getEncoded()));
	}
}
