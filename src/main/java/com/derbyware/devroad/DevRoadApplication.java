package com.derbyware.devroad;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.io.Encoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.security.Key;
import java.util.Arrays;

@SpringBootApplication
public class DevRoadApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevRoadApplication.class, args);
	}

}
