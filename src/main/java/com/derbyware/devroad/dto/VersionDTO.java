package com.derbyware.devroad.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;

@Data
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class VersionDTO {
	private long id;

	@JsonIgnoreProperties(value = {"versions", "children", "parent"})
	private TechnologyDTO technology;

	private String name;

	// Gets 05-2020 from React. show: May 2020
	private String releaseDate;
	// added by user

	private String createdAt;

	private String description;
}
