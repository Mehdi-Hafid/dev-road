package com.derbyware.devroad.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;

@Data
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class DisplayUpgradeChildDTO {

	private long id;

	private String acquired;

	private String name;

	private long versionId;

	private String versionName;

	private String versionReleaseDate;

}
