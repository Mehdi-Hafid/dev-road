package com.derbyware.devroad.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ProjectDTO {
	private long id;
	//user

	private String title;

	private String fromDate;

	private String toDate;

	private String detail;

	private List<UpgradeDTO> upgrades = new ArrayList<>();

	private String createdAt;
}
