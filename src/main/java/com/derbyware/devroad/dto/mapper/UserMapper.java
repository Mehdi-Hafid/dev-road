package com.derbyware.devroad.dto.mapper;

import com.derbyware.devroad.dto.UserDTO;
import com.derbyware.devroad.entities.User;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserMapper {

	UserDTO userToUserDto(User user, @Context CycleAvoidingMappingContext context);

	User userDtoToUser(UserDTO userDTO, @Context CycleAvoidingMappingContext context);
}
