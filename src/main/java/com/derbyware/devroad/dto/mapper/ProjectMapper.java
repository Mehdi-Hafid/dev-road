package com.derbyware.devroad.dto.mapper;

import com.derbyware.devroad.dto.ProjectDTO;
import com.derbyware.devroad.entities.Project;
import org.mapstruct.*;

@Mapper(componentModel = "spring", uses = {UpgradeMapper.class, YearMonthMapper.class})
public interface ProjectMapper {

	@Mapping(target = "createdAt", dateFormat = "dd/MMMM/yyyy [ 'at' HH:mm:ss]")
	@Mapping(target = "fromDate", dateFormat = "MMMM yyyy")
	@Mapping(target = "toDate", dateFormat = "MMMM yyyy")
	@Mapping(target = "upgrades", qualifiedByName = "withoutProjects")
	ProjectDTO projectToProjectDto(Project project, @Context CycleAvoidingMappingContext context);

	@Mapping(target = "upgrades", ignore = true)
	@Mapping(target = "createdAt", dateFormat = "dd/MMMM/yyyy [ 'at' HH:mm:ss]")
	@Mapping(target = "fromDate", dateFormat = "MMMM yyyy")
	@Mapping(target = "toDate", dateFormat = "MMMM yyyy")
	@Named("withoutUpgrades")
	ProjectDTO projectToProjectDtoWithoutUpgrades(Project project, @Context CycleAvoidingMappingContext context);

	@Mapping(target = "createdAt", dateFormat = "dd/MMMM/yyyy [ 'at' HH:mm:ss]")
	@Mapping(target = "fromDate", dateFormat = "MM-yyyy")
	@Mapping(target = "toDate", dateFormat = "MM-yyyy")
	Project projectDtoToProject(ProjectDTO projectDTO, @Context CycleAvoidingMappingContext context);
}
