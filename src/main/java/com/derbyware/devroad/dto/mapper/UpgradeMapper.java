package com.derbyware.devroad.dto.mapper;

import com.derbyware.devroad.dto.ProjectDTO;
import com.derbyware.devroad.dto.UpgradeDTO;
import com.derbyware.devroad.entities.Project;
import com.derbyware.devroad.entities.Upgrade;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {VersionMapper.class, ProjectMapper.class,
		YearMonthMapper.class})
public interface UpgradeMapper {

	@Mapping(target = "createdAt", dateFormat = "dd/MMMM/yyyy [ 'at' HH:mm:ss]")
	@Mapping(target = "acquired", dateFormat = "MMMM yyyy")
	@Mapping(target = "projects", qualifiedByName = "withoutUpgrades")
	UpgradeDTO upgradeToUpgradeDto(Upgrade upgrade, @Context CycleAvoidingMappingContext context);

	@Mapping(target = "projects", ignore = true)
	@Mapping(target = "createdAt", dateFormat = "dd/MMMM/yyyy [ 'at' HH:mm:ss]")
	@Mapping(target = "acquired", dateFormat = "MMMM yyyy")
	@Named("withoutProjects")
	UpgradeDTO upgradeToUpgradeDtoWithoutProjects(Upgrade upgrade, @Context CycleAvoidingMappingContext context);

	@Mapping(target = "createdAt", dateFormat = "dd/MMMM/yyyy [ 'at' HH:mm:ss]")
	@Mapping(target = "acquired", dateFormat = "MM-yyyy")
	Upgrade upgradeDtoToUpgrade(UpgradeDTO upgradeDTO, @Context CycleAvoidingMappingContext context);

}
