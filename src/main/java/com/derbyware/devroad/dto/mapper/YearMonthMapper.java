package com.derbyware.devroad.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

@Mapper(componentModel = "spring")
public interface YearMonthMapper {

	default String yearMonthToString(YearMonth releasedDate) {
		if(releasedDate == null) return null;
		return releasedDate.format(DateTimeFormatter.ofPattern("MMMM yyyy"));
	}

	default YearMonth stringToYearMonth(String releasedDate) {
		if(releasedDate == null) return null;
		YearMonth date = YearMonth.parse(releasedDate, DateTimeFormatter.ofPattern("MM-yyyy"));
		return date;
	}
}
