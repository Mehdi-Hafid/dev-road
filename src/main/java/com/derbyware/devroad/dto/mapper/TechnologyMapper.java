package com.derbyware.devroad.dto.mapper;

import com.derbyware.devroad.dto.TechnologyDTO;
import com.derbyware.devroad.dto.VersionDTO;
import com.derbyware.devroad.entities.Technology;
import com.derbyware.devroad.entities.Version;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = VersionMapper.class)
public interface TechnologyMapper {

	@Mapping(target = "createdAt", dateFormat = "dd/MMMM/yyyy [ 'at' HH:mm:ss]")
	TechnologyDTO technologyToTechnologyDto(Technology technology,
	                                        @Context CycleAvoidingMappingContext context);

	@Mapping(target = "createdAt", dateFormat = "dd/MMMM/yyyy [ 'at' HH:mm:ss]")
	Technology technologyDtoToTechnology(TechnologyDTO technologyDTO,
	                                     @Context CycleAvoidingMappingContext context);
}
