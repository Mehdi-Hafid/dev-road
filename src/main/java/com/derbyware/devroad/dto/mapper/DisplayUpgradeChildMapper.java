package com.derbyware.devroad.dto.mapper;

import com.derbyware.devroad.dto.DisplayUpgradeChildDTO;
import com.derbyware.devroad.entities.constructor.DisplayUpgradeChild;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {YearMonthMapper.class})
public interface DisplayUpgradeChildMapper {

	@Mapping(target = "acquired", dateFormat = "MMMM yyyy")
	@Mapping(target = "versionReleaseDate", dateFormat = "MMMM yyyy")
	DisplayUpgradeChildDTO DisplayUpgradeChildToDisplayUpgradeChildDTO(DisplayUpgradeChild child,
	                                                                   @Context CycleAvoidingMappingContext context);

}
