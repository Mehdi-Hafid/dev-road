package com.derbyware.devroad.dto.mapper;

import com.derbyware.devroad.dto.VersionDTO;
import com.derbyware.devroad.entities.Version;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {TechnologyMapper.class, YearMonthMapper.class})
public interface VersionMapper {

	@Mapping(target = "createdAt", dateFormat = "dd/MMMM/yyyy [ 'at' HH:mm:ss]")
	@Mapping(target = "releaseDate", dateFormat = "MMMM yyyy")
	VersionDTO versionToVersionDto(Version version, @Context CycleAvoidingMappingContext context);

	@Mapping(target = "createdAt", dateFormat = "dd/MMMM/yyyy [ 'at' HH:mm:ss]")
	@Mapping(target = "releaseDate", dateFormat = "MM-yyyy")
	Version versionDtoToVersion(VersionDTO versionDTO, @Context CycleAvoidingMappingContext context);


}
