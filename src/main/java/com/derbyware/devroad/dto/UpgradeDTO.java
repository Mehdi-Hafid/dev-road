package com.derbyware.devroad.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class UpgradeDTO {
	private long id;
	//user

	private VersionDTO version;

	private String acquired;

	private String name;

	private String books;

	private String courses;

	private String achievements;

	private List<ProjectDTO> projects = new ArrayList<>();

	private String createdAt;

	private String description;
}
