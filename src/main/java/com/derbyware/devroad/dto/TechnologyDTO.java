package com.derbyware.devroad.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@ToString(exclude = {"versions", "children"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class TechnologyDTO {
	private long id;

	private String name;

	private String seoName;

	private List<VersionDTO> versions = new ArrayList<>();

	private String imageLink;
	// added by user

	@JsonIgnoreProperties(value = {"versions", "parent", "children"})
	private TechnologyDTO parent;

	private List<TechnologyDTO> children = new ArrayList<>();

	private String createdAt;

	private String description;
}
