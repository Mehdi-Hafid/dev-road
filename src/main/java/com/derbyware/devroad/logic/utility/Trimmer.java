package com.derbyware.devroad.logic.utility;

import com.derbyware.devroad.dto.ProjectDTO;
import com.derbyware.devroad.dto.TechnologyDTO;
import com.derbyware.devroad.dto.UpgradeDTO;
import com.derbyware.devroad.dto.VersionDTO;
import org.apache.commons.text.WordUtils;

import java.util.Objects;

public class Trimmer {

	public static void trim(TechnologyDTO technologyDTO) {
		if(Objects.nonNull(technologyDTO.getName())){
			technologyDTO.setName(technologyDTO.getName().trim());
		}
		if(Objects.nonNull(technologyDTO.getImageLink())){
			technologyDTO.setImageLink(technologyDTO.getImageLink().trim());
		}

	}

	public static void trim(VersionDTO versionDTO) {
		if(Objects.nonNull(versionDTO.getName())){
			versionDTO.setName(versionDTO.getName().trim());
		}
	}

	public static void trim(UpgradeDTO upgradeDTO) {
		if(Objects.nonNull(upgradeDTO.getName())){
			upgradeDTO.setName(WordUtils.capitalize(upgradeDTO.getName().trim()));
		}

	}

	public static void trim(ProjectDTO projectDTO) {
		if(Objects.nonNull(projectDTO.getTitle())){
			projectDTO.setTitle(WordUtils.capitalize(projectDTO.getTitle().trim()));
		}
	}

}
