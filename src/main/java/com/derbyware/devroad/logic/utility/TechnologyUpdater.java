package com.derbyware.devroad.logic.utility;

import com.derbyware.devroad.entities.Technology;
import com.derbyware.devroad.repositories.TechnologyRepository;

import java.util.Objects;

public class TechnologyUpdater {
	public static void update(Technology old, Technology updated, TechnologyRepository technologyRepository) {

		updateName(old, updated);
		updateImageLink(old, updated);
		updateDescription(old, updated);
		updateParent(old, updated, technologyRepository);
	}

	private static void updateName(Technology old, Technology updated){
		if (Objects.isNull(old.getName()) && Objects.isNull(updated.getName())) {
			return;
		} else if(Objects.isNull(updated.getName())){
			return;
		} else if (Objects.isNull(old.getName()) && Objects.nonNull(updated.getName())) {
			old.setName(updated.getName());
		} else {
			if (!old.getName().equals(updated.getName())) {
				old.setName(updated.getName());
			}
		}
	}

	// null values means no update
	private static void updateImageLink(Technology old, Technology updated){
		if (Objects.isNull(old.getImageLink()) && Objects.isNull(updated.getImageLink())) {
			return;
		} else if(Objects.isNull(updated.getImageLink())){
			return;
		} else if (Objects.isNull(old.getImageLink()) && Objects.nonNull(updated.getImageLink())) {
			old.setImageLink(updated.getImageLink());
		} else {
			if (!old.getImageLink().equals(updated.getImageLink())) {
				old.setImageLink(updated.getImageLink());
			}
		}
	}

	private static void updateDescription(Technology old, Technology updated) {
		if (Objects.isNull(old.getDescription()) && Objects.isNull(updated.getDescription())) {
			return;
		} else if(Objects.isNull(updated.getDescription())){
			return;
		} else if (Objects.isNull(old.getDescription()) && Objects.nonNull(updated.getDescription())) {
			old.setDescription(updated.getDescription());
		} else {
			if (!old.getDescription().equals(updated.getDescription())) {
				old.setDescription(updated.getDescription());
			}
		}
	}

	private static void updateParent(Technology old, Technology updated, TechnologyRepository technologyRepository){
		Technology parent;
		if (Objects.isNull(old.getParent()) && Objects.isNull(updated.getParent())) {
			return;
		} else if(Objects.isNull(updated.getParent())){
			return;
		} else if (Objects.isNull(old.getParent()) && Objects.nonNull(updated.getParent())) {
			parent = technologyRepository.findById(updated.getParent().getId()).get();
			old.setParent(parent);
		} else {
			if (!old.getParent().equals(updated.getParent())) {
				parent = technologyRepository.findById(updated.getParent().getId()).get();
				old.setParent(parent);
			}
		}
	}
}
