package com.derbyware.devroad.logic.utility;

public class SEOName {

	public static String make(String name){
		return name.trim().replace(" ", "-").toLowerCase();
	}
}
