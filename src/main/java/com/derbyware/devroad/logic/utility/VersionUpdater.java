package com.derbyware.devroad.logic.utility;

import com.derbyware.devroad.entities.Technology;
import com.derbyware.devroad.entities.Version;
import com.derbyware.devroad.repositories.TechnologyRepository;
import com.derbyware.devroad.repositories.VersionRepository;

import java.util.Objects;

public class VersionUpdater {
	public static void update(Version old, Version updated, TechnologyRepository technologyRepository) {

		updateName(old, updated);
		updateReleaseDate(old, updated);
		updateDescription(old, updated);
		updateTechnology(old, updated, technologyRepository);
	}

	private static void updateName(Version old, Version updated){
		if (Objects.isNull(old.getName()) && Objects.isNull(updated.getName())) {
			return;
		} else if(Objects.isNull(updated.getName())){
			return;
		} else if (Objects.isNull(old.getName()) && Objects.nonNull(updated.getName())) {
			old.setName(updated.getName());
		} else {
			if (!old.getName().equals(updated.getName())) {
				old.setName(updated.getName());
			}
		}
	}

	// null values means no update
	private static void updateReleaseDate(Version old, Version updated){
		if (Objects.isNull(old.getReleaseDate()) && Objects.isNull(updated.getReleaseDate())) {
			return;
		} else if(Objects.isNull(updated.getReleaseDate())){
			return;
		} else if (Objects.isNull(old.getReleaseDate()) && Objects.nonNull(updated.getReleaseDate())) {
			old.setReleaseDate(updated.getReleaseDate());
		} else {
			if (!old.getReleaseDate().equals(updated.getReleaseDate())) {
				old.setReleaseDate(updated.getReleaseDate());
			}
		}
	}

	private static void updateDescription(Version old, Version updated) {
		if (Objects.isNull(old.getDescription()) && Objects.isNull(updated.getDescription())) {
			return;
		} else if(Objects.isNull(updated.getDescription())){
			return;
		} else if (Objects.isNull(old.getDescription()) && Objects.nonNull(updated.getDescription())) {
			old.setDescription(updated.getDescription());
		} else {
			if (!old.getDescription().equals(updated.getDescription())) {
				old.setDescription(updated.getDescription());
			}
		}
	}

	private static void updateTechnology(Version old, Version updated, TechnologyRepository technologyRepository){
		Technology technology;
		if (Objects.isNull(old.getTechnology()) && Objects.isNull(updated.getTechnology())) {
			return;
		} else if(Objects.isNull(updated.getTechnology())){
			return;
		} else if (Objects.isNull(old.getTechnology()) && Objects.nonNull(updated.getTechnology())) {
			technology = technologyRepository.findById(updated.getTechnology().getId()).get();
			old.setTechnology(technology);
		} else {
			if (!old.getTechnology().equals(updated.getTechnology())) {
				technology = technologyRepository.findById(updated.getTechnology().getId()).get();
				old.setTechnology(technology);
			}
		}
	}
}
