package com.derbyware.devroad.logic.utility;

import com.derbyware.devroad.entities.Project;
import com.derbyware.devroad.entities.Upgrade;
import com.derbyware.devroad.entities.Version;
import com.derbyware.devroad.repositories.ProjectRepository;
import com.derbyware.devroad.repositories.VersionRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class UpgradeUpdater {

	public static void update(Upgrade old, Upgrade updated, VersionRepository versionRepository,
	                          ProjectRepository projectRepository) {

		updateName(old, updated);
		updateAcquired(old, updated);
		updateVersion(old, updated, versionRepository);
		updateBooks(old, updated);
		updateCourses(old, updated);
		updateAchievements(old, updated);
		updateProjects(old, updated, projectRepository);
	}

	private static void updateName(Upgrade old, Upgrade updated){
		if (Objects.isNull(old.getName()) && Objects.isNull(updated.getName())) {
			return;
		} else if(Objects.isNull(updated.getName())){
			return;
		} else if (Objects.isNull(old.getName()) && Objects.nonNull(updated.getName())) {
			old.setName(updated.getName());
		} else {
			if (!old.getName().equals(updated.getName())) {
				old.setName(updated.getName());
			}
		}
	}

	private static void updateAcquired(Upgrade old, Upgrade updated){
		if (Objects.isNull(old.getAcquired()) && Objects.isNull(updated.getAcquired())) {
			return;
		} else if(Objects.isNull(updated.getAcquired())){
			return;
		} else if (Objects.isNull(old.getAcquired()) && Objects.nonNull(updated.getAcquired())) {
			old.setAcquired(updated.getAcquired());
		} else {
			if (!old.getAcquired().equals(updated.getAcquired())) {
				old.setAcquired(updated.getAcquired());
			}
		}
	}

	private static void updateVersion(Upgrade old, Upgrade updated, VersionRepository versionRepository) {
		Version version;
		if (Objects.isNull(old.getVersion()) && Objects.isNull(updated.getVersion())) {
			return;
		} else if(Objects.isNull(updated.getVersion())){
			return;
		} else if (Objects.isNull(old.getVersion()) && Objects.nonNull(updated.getVersion())) {
			version = versionRepository.findById(updated.getVersion().getId()).get();
			old.setVersion(version);
		} else {
			if (!old.getVersion().equals(updated.getVersion())) {
				version = versionRepository.findById(updated.getVersion().getId()).get();
				old.setVersion(version);
			}
		}
	}

	private static void updateBooks(Upgrade old, Upgrade updated) {
		if (Objects.isNull(old.getBooks()) && Objects.isNull(updated.getBooks())) {
			return;
		} else if(Objects.isNull(updated.getBooks())){
			return;
		} else if (Objects.isNull(old.getBooks()) && Objects.nonNull(updated.getBooks())) {
			old.setBooks(updated.getBooks());
		} else {
			if (!old.getBooks().equals(updated.getBooks())) {
				old.setBooks(updated.getBooks());
			}
		}
	}

	private static void updateCourses(Upgrade old, Upgrade updated) {
		if (Objects.isNull(old.getCourses()) && Objects.isNull(updated.getCourses())) {
			return;
		} else if(Objects.isNull(updated.getCourses())){
			return;
		} else if (Objects.isNull(old.getCourses()) && Objects.nonNull(updated.getCourses())) {
			old.setCourses(updated.getCourses());
		} else {
			if (!old.getCourses().equals(updated.getCourses())) {
				old.setCourses(updated.getCourses());
			}
		}
	}

	private static void updateAchievements(Upgrade old, Upgrade updated) {
		if (Objects.isNull(old.getAchievements()) && Objects.isNull(updated.getAchievements())) {
			return;
		} else if(Objects.isNull(updated.getAchievements())){
			return;
		} else if (Objects.isNull(old.getAchievements()) && Objects.nonNull(updated.getAchievements())) {
			old.setAchievements(updated.getAchievements());
		} else {
			if (!old.getAchievements().equals(updated.getAchievements())) {
				old.setAchievements(updated.getCourses());
			}
		}
	}

	// assumed replace old projects with new sent ones
	private static void updateProjects(Upgrade old, Upgrade updated, ProjectRepository projectRepository) {

		if (updated.getProjects().isEmpty()) {
			old.getProjects().clear();
			return;
		} else {
			List<Project> projects = new ArrayList<>();
			updated.getProjects().forEach(project -> {
				Project projectEntity = projectRepository.findById(project.getId()).get();
				projects.add(projectEntity);
//			results.add(project);
			});
			old.getProjects().clear();
			old.getProjects().addAll(projects);
		}
	}

}
