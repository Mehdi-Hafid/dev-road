package com.derbyware.devroad.logic.auth;

import lombok.Data;

@Data
public class AccountCredentials {
	private String email;
	private String password;

}
