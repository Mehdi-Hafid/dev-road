package com.derbyware.devroad.logic.auth;

import com.derbyware.devroad.startup.KeyWriterReader;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static java.util.Collections.emptyList;

@Slf4j
public class AuthenticationService {
	private static final long EXPIRATIONTIME = 86_400_000; // 1 day in milliseconds
	private static final Key key = KeyWriterReader.key;
	private static final String PREFIX = "Bearer";
	private static final String AUTHORIZATION_HEADER = "Authorization";

	static public void addToken(HttpServletResponse res, String username) {
		Date expiration = new Date(System.currentTimeMillis() + EXPIRATIONTIME);
		String JwtToken =
				Jwts.builder().setSubject(username)
						.setExpiration(expiration)
						.signWith(key, SignatureAlgorithm.HS512).compact();
		res.addHeader(AUTHORIZATION_HEADER, PREFIX + " " + JwtToken);
		res.addHeader("Access-Control-Expose-Headers", AUTHORIZATION_HEADER);
		res.addHeader("Expires", expiration.toString()); // for front only
	}

	static public Authentication getAuthentication(HttpServletRequest request) {
		String token = request.getHeader(AUTHORIZATION_HEADER);
		log.info("Token: " + token);
		if (token.startsWith(PREFIX)) {
			Jws<Claims> claim = Jwts.parserBuilder().setSigningKey(key).build()
					.parseClaimsJws(token.replace(PREFIX, ""));
			String user = claim.getBody().getSubject();
			if(user == null){
				log.info("user is null");
				return null;
			}
			Date expirationDate = claim.getBody().getExpiration();
			if(expirationDate != null){
				LocalDateTime date = LocalDateTime.ofInstant(
						claim.getBody().getExpiration().toInstant(), ZoneId.systemDefault());
				if(date.isBefore(LocalDateTime.now())){
					log.info("Expired Token");
					return null;
				}
			}
			return new UsernamePasswordAuthenticationToken(user, null, emptyList());
		}
		return null;
	}
}