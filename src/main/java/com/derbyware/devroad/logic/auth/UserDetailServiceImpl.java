package com.derbyware.devroad.logic.auth;

import com.derbyware.devroad.entities.User;
import com.derbyware.devroad.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class UserDetailServiceImpl implements UserDetailsService {

	private final UserRepository repository;


	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		User currentUser = repository.findByEmail(email);
		System.out.println("currentUser : " + currentUser);
		if(currentUser == null){
			throw new UsernameNotFoundException("Email not found");
		}
		UserDetails user = new org.springframework.security.core.userdetails.User(email,
				currentUser.getPassword(), true, true, true, true,
				AuthorityUtils.createAuthorityList(currentUser.getRole()));
		return user;
	}

}