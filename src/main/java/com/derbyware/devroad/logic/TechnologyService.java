package com.derbyware.devroad.logic;

import com.derbyware.devroad.dto.TechnologyDTO;
import com.derbyware.devroad.dto.mapper.CycleAvoidingMappingContext;
import com.derbyware.devroad.dto.mapper.TechnologyMapper;
import com.derbyware.devroad.entities.Technology;
import com.derbyware.devroad.logic.utility.SEOName;
import com.derbyware.devroad.logic.utility.TechnologyUpdater;
import com.derbyware.devroad.logic.utility.Trimmer;
import com.derbyware.devroad.repositories.TechnologyRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class TechnologyService {

	private final TechnologyRepository technologyRepository;

	private final TechnologyMapper technologyMapper;

	public TechnologyDTO createTechnology(TechnologyDTO technologyDTO) {

		Trimmer.trim(technologyDTO);
		Technology technology = technologyMapper.technologyDtoToTechnology(technologyDTO,
				new CycleAvoidingMappingContext());
		setup(technology, false);
		technology = technologyRepository.save(technology);

		if(Objects.nonNull(technologyDTO.getParent())){
			long parentId = technologyDTO.getParent().getId();
			technology.setParent(technologyRepository.findById(parentId).get());
		} else {
			technology.setParent(null);
		}

		return technologyMapper.technologyToTechnologyDto(technology,
				new CycleAvoidingMappingContext());
	}

	private void setup(Technology technology, boolean updating) {
		technology.setSeoName(SEOName.make(technology.getName()));
		if (!updating) {
			technology.setCreatedAt(LocalDateTime.now());
		}
	}

	public TechnologyDTO update(long id, TechnologyDTO updated) {
		Optional<Technology> existingTechnologyOp = technologyRepository.findById(id);
		Technology existingTechnology = null;
		if (existingTechnologyOp.isPresent()) {
			existingTechnology = existingTechnologyOp.get();
			Trimmer.trim(updated);
			Technology newTechnology = technologyMapper.technologyDtoToTechnology(updated,
					new CycleAvoidingMappingContext());
			TechnologyUpdater.update(existingTechnology, newTechnology, technologyRepository);
			existingTechnology = technologyRepository.save(existingTechnology);
			return technologyMapper.technologyToTechnologyDto(existingTechnology,
					new CycleAvoidingMappingContext());
		} else {
			// handle exception
			return null;
		}
	}

//	private void processUpdate(Technology old, Technology updated) {
//		TechnologyUpdater.update(old, updated);
//	}

	public TechnologyDTO replace(long id, TechnologyDTO updated) {
		Trimmer.trim(updated);
		Technology updatedTechnology = technologyMapper.technologyDtoToTechnology(updated,
				new CycleAvoidingMappingContext());
		updatedTechnology.setId(id);
		setup(updatedTechnology, true);
		LocalDateTime createdAt = technologyRepository.findById(id).get().getCreatedAt();
		updatedTechnology.setCreatedAt(createdAt);
		updatedTechnology = technologyRepository.save(updatedTechnology);
		return technologyMapper.technologyToTechnologyDto(updatedTechnology,
				new CycleAvoidingMappingContext());
	}

	public void delete(long id) {
		technologyRepository.deleteById(id);
	}

	public TechnologyDTO get(long id) {
		Optional<Technology> technology = technologyRepository.findById(id);
		return technology.map(value -> technologyMapper.technologyToTechnologyDto(value,
				new CycleAvoidingMappingContext())).orElse(null);
	}

	public TechnologyDTO get(String seoname) {
		Optional<Technology> technology = technologyRepository.findBySeoName(seoname);
		return technology.map(value -> technologyMapper.technologyToTechnologyDto(value,
				new CycleAvoidingMappingContext())).orElse(null);
	}

	public List<TechnologyDTO> recent(int page) {
		PageRequest pageRequest = PageRequest.of(page, 100, Sort.by("createdAt").descending());
		return technologyRepository.findAll(pageRequest).stream().map(technology -> {
			return technologyMapper.technologyToTechnologyDto(technology,
					new CycleAvoidingMappingContext());
		}).collect(Collectors.toList());
	}

	public TechnologyDTO nullify(long id, Map<String, Object> updates){
		Technology technology = technologyRepository.findById(id).get();
		updates.forEach((proprety, value) -> nullify(proprety, technology));
		Technology result = technologyRepository.save(technology);
		return technologyMapper.technologyToTechnologyDto(technology,
				new CycleAvoidingMappingContext());
	}

	private void nullify(String propertyName, Technology technology){
		switch (propertyName){
			// this should be illegal
			case "name":
//				technology.setName(null);
//				technology.setSeoName(null);
				break;
			case "imageLink":
				technology.setImageLink(null);
				break;
			case "parent":
				technology.setParent(null);
				break;
			case "description":
				technology.setDescription(null);
				break;
			default:
				throw new IllegalStateException("Unexpected value: " + propertyName);
		}
	}

}
