package com.derbyware.devroad.logic;

import com.derbyware.devroad.dto.ProjectDTO;
import com.derbyware.devroad.dto.mapper.CycleAvoidingMappingContext;
import com.derbyware.devroad.dto.mapper.ProjectMapper;
import com.derbyware.devroad.entities.Project;
import com.derbyware.devroad.logic.utility.Trimmer;
import com.derbyware.devroad.repositories.ProjectRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class ProjectService {

	private final ProjectMapper projectMapper;

	private final ProjectRepository projectRepository;

	public ProjectDTO createProject(ProjectDTO projectDTO) {
		Trimmer.trim(projectDTO);
		Project project = projectMapper.projectDtoToProject(projectDTO,
				new CycleAvoidingMappingContext());
		setup(project);
		project = projectRepository.save(project);
		return projectMapper.projectToProjectDto(project, new CycleAvoidingMappingContext());
	}

	private void setup(Project project) {
		project.setCreatedAt(LocalDateTime.now());
	}

	public ProjectDTO get(long id) {
		Optional<Project> projectOp = projectRepository.findById(id);
		return projectOp.map(value -> projectMapper.projectToProjectDto(value,
				new CycleAvoidingMappingContext())).orElse(null);
	}

	public List<ProjectDTO> recent(int page) {
		PageRequest pageRequest = PageRequest.of(page, 10, Sort.by("createdAt").descending());
		return projectRepository.findAll(pageRequest).stream().map(project -> {
			return projectMapper.projectToProjectDto(project, new CycleAvoidingMappingContext());
		}).collect(Collectors.toList());
	}

	public ProjectDTO replace(long id, ProjectDTO updated) {
		Trimmer.trim(updated);
		Project updatedProject = projectMapper.projectDtoToProject(updated,
				new CycleAvoidingMappingContext());
		updatedProject.setId(id);
		LocalDateTime createdAt = projectRepository.findById(id).get().getCreatedAt();
		updatedProject.setCreatedAt(createdAt);
		updatedProject = projectRepository.save(updatedProject);
		return projectMapper.projectToProjectDto(updatedProject, new CycleAvoidingMappingContext());
	}

	public void delete(long id) {
		Optional<Project> projectOp = projectRepository.findById(id);
		if (projectOp.isPresent()) {
			projectRepository.deleteById(id);
		}
	}

}
