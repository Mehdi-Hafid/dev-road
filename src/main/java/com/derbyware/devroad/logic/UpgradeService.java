package com.derbyware.devroad.logic;

import com.derbyware.devroad.dto.DisplayUpgradeChildDTO;
import com.derbyware.devroad.dto.UpgradeDTO;
import com.derbyware.devroad.dto.mapper.CycleAvoidingMappingContext;
import com.derbyware.devroad.dto.mapper.DisplayUpgradeChildMapper;
import com.derbyware.devroad.dto.mapper.UpgradeMapper;
import com.derbyware.devroad.entities.Project;
import com.derbyware.devroad.entities.Technology;
import com.derbyware.devroad.entities.Upgrade;
import com.derbyware.devroad.entities.Version;
import com.derbyware.devroad.entities.constructor.DisplayUpgrade;
import com.derbyware.devroad.entities.constructor.DisplayUpgradeChild;
import com.derbyware.devroad.logic.utility.Trimmer;
import com.derbyware.devroad.logic.utility.UpgradeUpdater;
import com.derbyware.devroad.repositories.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.Named;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class UpgradeService {

	private final UpgradeMapper upgradeMapper;

	private final VersionRepository versionRepository;

	private final UpgradeRepository upgradeRepository;

	private final DisplayUpgradeRepository displayUpgradeRepository;

	private final DisplayUpgradeChildMapper displayUpgradeChildMapper;

	private final ProjectRepository projectRepository;

	private final TechnologyRepository technologyRepository;

	public UpgradeDTO createUpgrade(long versionId, UpgradeDTO upgradeDTO) {
		Trimmer.trim(upgradeDTO);
		CycleAvoidingMappingContext cycleAvoidingMappingContext = new CycleAvoidingMappingContext();
		Upgrade upgrade = upgradeMapper.upgradeDtoToUpgrade(upgradeDTO, cycleAvoidingMappingContext);
		upgrade = setupAndSave(versionId, upgrade);
		return upgradeMapper.upgradeToUpgradeDto(upgrade, cycleAvoidingMappingContext);
	}

	private Upgrade setupAndSave(long versionId, Upgrade upgrade) {
		Optional<Version> versionOp = versionRepository.findById(versionId);
		if (!versionOp.isPresent()) {
			return null; // To be handled
		}
		Version version = versionOp.get();
		upgrade.setVersion(version);
		upgrade.setCreatedAt(LocalDateTime.now());
		// TODO projects
		List<Project> projects = addProjects(upgrade);
		upgrade = upgradeRepository.save(upgrade);
		log.info("saved upgrade: " + upgrade);

		syncProjects(projects, upgrade);
		return upgradeRepository.findById(upgrade.getId()).get();
	}

	private List<Project> addProjects( Upgrade upgrade) {
		List<Project> results = new ArrayList<>();
		upgrade.getProjects().forEach(project -> {
			Project projectEntity = projectRepository.findById(project.getId()).get();
			log.info("Project: " + projectEntity);
			results.add(projectEntity);
//			results.add(project);
		});
		upgrade.getProjects().clear();
		upgrade.getProjects().addAll(results);
		return results;
	}

	private void syncProjects(List<Project> projects, Upgrade upgrade) {
		projects.forEach(project -> {
			project.getUpgrades().add(upgrade);
			projectRepository.save(project);
		});
	}

	public UpgradeDTO get(long id) {
		Optional<Upgrade> upgrade = upgradeRepository.findById(id);
		return upgrade.map(value -> upgradeMapper.upgradeToUpgradeDto(value,
				new CycleAvoidingMappingContext())).orElse(null);
	}

	public DisplayUpgrade getAll(long technologyId) {
		DisplayUpgrade displayUpgrade = displayUpgradeRepository.getTechnologyData(technologyId);
		List<DisplayUpgradeChild> upgradesInfo =
				displayUpgradeRepository.getUpgradesInfo(technologyId);
		List<DisplayUpgradeChildDTO> upgradesInfoDTO =
				upgradesInfo.stream().map(child -> displayUpgradeChildMapper.DisplayUpgradeChildToDisplayUpgradeChildDTO(child, new CycleAvoidingMappingContext())).collect(Collectors.toList());
		displayUpgrade.setUpgrades(upgradesInfoDTO);
		return displayUpgrade;
	}

	public DisplayUpgrade getAll(String technologySeoName){
		DisplayUpgrade displayUpgrade;
		Optional<Technology> technologyOp = technologyRepository.findBySeoName(technologySeoName);
		if(technologyOp.isPresent()){
			long id = technologyOp.get().getId();
			displayUpgrade = getAll(id);
			return displayUpgrade;
		}
		return null;
	}

	public UpgradeDTO update(long id, UpgradeDTO updated) {
		Optional<Upgrade> existingUpgradeOp = upgradeRepository.findById(id);
		if (existingUpgradeOp.isPresent()) {
			Upgrade existingUpgrade = existingUpgradeOp.get();
			Trimmer.trim(updated);
			Upgrade newUpgrade = upgradeMapper.upgradeDtoToUpgrade(updated,
					new CycleAvoidingMappingContext());
			UpgradeUpdater.update(existingUpgrade, newUpgrade, versionRepository, projectRepository);
			existingUpgrade = upgradeRepository.save(existingUpgrade);
			syncProjects(existingUpgrade.getProjects(), existingUpgrade);
			return upgradeMapper.upgradeToUpgradeDto(existingUpgrade,
					new CycleAvoidingMappingContext());
		} else {
			// handle exception
			return null;
		}
	}

	private void processUpdate(Upgrade old, Upgrade updated) {
		// update projects differently
	}

	public UpgradeDTO replace(long id, UpgradeDTO updated) {
		long versionId = updated.getVersion().getId();
		updated.setVersion(null);
		LocalDateTime createdAt = upgradeRepository.findById(id).get().getCreatedAt();
		Trimmer.trim(updated);
		Upgrade updatedUpgrade = upgradeMapper.upgradeDtoToUpgrade(updated,
				new CycleAvoidingMappingContext());
		updatedUpgrade.setId(id);
		updatedUpgrade.setCreatedAt(createdAt);
		updatedUpgrade.setVersion(versionRepository.findById(versionId).get());
		updatedUpgrade = upgradeRepository.save(updatedUpgrade);
		return upgradeMapper.upgradeToUpgradeDto(updatedUpgrade, new CycleAvoidingMappingContext());
	}

	public void delete(long id) {
		Optional<Upgrade> upgradeOp = upgradeRepository.findById(id);
		if (upgradeOp.isPresent()) {
			upgradeRepository.deleteById(id);
		}
	}

}
