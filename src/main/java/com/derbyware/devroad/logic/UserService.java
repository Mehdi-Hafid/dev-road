package com.derbyware.devroad.logic;

import com.derbyware.devroad.dto.UserDTO;
import com.derbyware.devroad.dto.mapper.CycleAvoidingMappingContext;
import com.derbyware.devroad.dto.mapper.UserMapper;
import com.derbyware.devroad.entities.User;
import com.derbyware.devroad.logic.utility.Trimmer;
import com.derbyware.devroad.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

@Slf4j
@RequiredArgsConstructor
@Service
public class UserService {

	private final UserMapper userMapper;

	private final UserRepository userRepository;

	private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

//	@Value("${server.address}")
//	private String serverAddress;

//	@Value("${server.port}")
//	private String serverPort;
	
	public UserDTO createUser(UserDTO userDTO) {

//		Trimmer.trim(userDTO);
		User user = userMapper.userDtoToUser(userDTO, new CycleAvoidingMappingContext());
		setup(user);
		user = userRepository.save(user);
		UserDTO userDTO1 = userMapper.userToUserDto(user, new CycleAvoidingMappingContext());

//		RestTemplate rest = new RestTemplate();
//		String loginEndpointValue = "http://localhost:8080/api/login";
//		log.info(loginEndpointValue);
//		ResponseEntity<UserDTO> userDTOResponseEntity = rest.postForEntity(loginEndpointValue,
//				userDTO1, UserDTO.class);
//		userDTOResponseEntity.getHeaders().get("Authorization");
//		userDTOResponseEntity.getHeaders().get("Expires");
		return userDTO1;
	}

	private void setup(User user){
		String encoded = bCryptPasswordEncoder.encode(user.getPassword());
		log.info(user.getPassword() + " becomes : " + encoded);
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setRole("User");
	}
}
