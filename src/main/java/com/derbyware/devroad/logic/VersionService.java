package com.derbyware.devroad.logic;

import com.derbyware.devroad.dto.TechnologyDTO;
import com.derbyware.devroad.dto.VersionDTO;
import com.derbyware.devroad.dto.mapper.CycleAvoidingMappingContext;
import com.derbyware.devroad.dto.mapper.VersionMapper;
import com.derbyware.devroad.entities.Technology;
import com.derbyware.devroad.entities.Version;
import com.derbyware.devroad.logic.utility.TechnologyUpdater;
import com.derbyware.devroad.logic.utility.Trimmer;
import com.derbyware.devroad.logic.utility.VersionUpdater;
import com.derbyware.devroad.repositories.TechnologyRepository;
import com.derbyware.devroad.repositories.VersionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class VersionService {

	private final TechnologyRepository technologyRepository;

	private final VersionRepository versionRepository;

	private final VersionMapper versionMapper;

	public VersionDTO createVersion(long technologyId, VersionDTO versionDTO) {
		Trimmer.trim(versionDTO);
		Version version = versionMapper.versionDtoToVersion(versionDTO,
				new CycleAvoidingMappingContext());
		version = setupAndSave(technologyId, version);
		return versionMapper.versionToVersionDto(version, new CycleAvoidingMappingContext());
	}

	private Version setupAndSave(long technologyId, Version version) {
		Optional<Technology> technologyOp = technologyRepository.findById(technologyId);
		if (!technologyOp.isPresent()) {
			return null; // To be handled
		}
		Technology technology = technologyOp.get();
		version.setTechnology(technology);
		version.setCreatedAt(LocalDateTime.now());
		version = versionRepository.save(version);
		technology.getVersions().add(version);
		technologyRepository.save(technology);
		return version;
	}

	public VersionDTO update(long id, VersionDTO updated) {
		Optional<Version> existingVersionOp = versionRepository.findById(id);
		if (existingVersionOp.isPresent()) {
			Version existingVersion = existingVersionOp.get();
			// handle here
			Trimmer.trim(updated);
			Version newVersion = versionMapper.versionDtoToVersion(updated,
					new CycleAvoidingMappingContext());
			VersionUpdater.update(existingVersion, newVersion, technologyRepository);
			existingVersion = versionRepository.save(existingVersion);
			return versionMapper.versionToVersionDto(existingVersion,
					new CycleAvoidingMappingContext());
		} else {
			// handle exception
			return null;
		}
	}

//	private void processUpdate(Version old, Version updated) {
//		if (!old.getName().equals(updated.getName())) {
//			old.setName(updated.getName());
//		}
//		if (!old.getReleaseDate().equals(updated.getReleaseDate())) {
//			old.setReleaseDate(updated.getReleaseDate());
//		}
//	}

//	public VersionDTO replace(long id, VersionDTO updated) {
//		Technology updatedTechnology = versionMapper.versionDtoToVersion(Trimmer.trim(updated));
//		System.out.println("ID is : " + id);
//		updatedTechnology.setId(id);
//		setup(updatedTechnology);
//		updatedTechnology = versionRepository.save(updatedTechnology);
//		return versionMapper.versionToVersionDto(updatedTechnology);
//	}

	public void delete(long id) {
		Optional<Version> versionOp = versionRepository.findById(id);
		if (versionOp.isPresent()) {
			Version version = versionOp.get();
			Technology technology = version.getTechnology();
			technology.getVersions().remove(version);
			technologyRepository.save(technology);
			versionRepository.deleteById(id);
		}
	}

	public VersionDTO get(long id) {
		Optional<Version> version = versionRepository.findById(id);
		return version.map(value -> versionMapper.versionToVersionDto(value,
				new CycleAvoidingMappingContext())).orElse(null);
	}

	public List<VersionDTO> recent(long technologyId, int page) {
		PageRequest pageRequest = PageRequest.of(page, 10, Sort.by("createdAt").descending());
		return versionRepository.findByTechnology_Id(technologyId, pageRequest).stream().map(version -> {
			return versionMapper.versionToVersionDto(version, new CycleAvoidingMappingContext());
		}).map(versionDTO -> {
			versionDTO.setTechnology(null);
			return versionDTO;
		}).collect(Collectors.toList());
	}

	public VersionDTO nullify(long id, Map<String, Object> updates){
		Version version = versionRepository.findById(id).get();
		updates.forEach((proprety, value) -> nullify(proprety, version));
		Version result = versionRepository.save(version);
		return versionMapper.versionToVersionDto(version, new CycleAvoidingMappingContext());
	}

	private void nullify(String propertyName, Version version){
		switch (propertyName){
			case "description":
				version.setDescription(null);
				break;
			default:
				throw new IllegalStateException("Unexpected value: " + propertyName);
		}
	}

}
