package com.derbyware.devroad.repositories;


import com.derbyware.devroad.entities.constructor.DisplayUpgrade;
import com.derbyware.devroad.entities.constructor.DisplayUpgradeChild;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.List;

@Slf4j
@Repository
public class DisplayUpgradeRepository {

	@PersistenceContext
	private EntityManager em;

	private static final String GET_ALL = "SELECT NEW com.derbyware.devroad.entities.constructor" +
			".DisplayUpgradeChild" + "( u.id, u.acquired, u.name, v.id, v.name, v.releaseDate) " +
			"FROM Upgrade u JOIN u.version v JOIN v.technology t " + "WHERE t.id = :id ORDER BY u" +
			".acquired DESC";

	private static final String GET_TECHNOLOGY_DATA = "SELECT NEW com.derbyware.devroad.entities" +
			".constructor.DisplayUpgrade" + "( t.id, t.name, t.seoName, t.imageLink) " + "FROM Technology t " +
			"WHERE t.id = :id";


	public DisplayUpgrade getTechnologyData(long id) {
		DisplayUpgrade result = null;
		try {
			result =
					em.createQuery(GET_TECHNOLOGY_DATA, DisplayUpgrade.class).setParameter("id", id).getSingleResult();
		} catch (NoResultException e) {
			e.printStackTrace();
		}
		log.info("result : " + result);
		return result;
	}

	public List<DisplayUpgradeChild> getUpgradesInfo(long id) {
		List<DisplayUpgradeChild> results =
				em.createQuery(GET_ALL, DisplayUpgradeChild.class).setParameter("id", id).getResultList();
		results.forEach(dto -> log.info("displayUpgradeChild : " + dto));
		return results;
	}

}
