package com.derbyware.devroad.repositories;

import com.derbyware.devroad.entities.Upgrade;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UpgradeRepository extends JpaRepository<Upgrade, Long> {

}
