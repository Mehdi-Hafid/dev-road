package com.derbyware.devroad.repositories;

import com.derbyware.devroad.entities.Technology;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TechnologyRepository extends JpaRepository<Technology, Long> {

	Optional<Technology> findBySeoName(String seoName);
}
