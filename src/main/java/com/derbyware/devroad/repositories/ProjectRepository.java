package com.derbyware.devroad.repositories;

import com.derbyware.devroad.entities.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<Project, Long> {
}
