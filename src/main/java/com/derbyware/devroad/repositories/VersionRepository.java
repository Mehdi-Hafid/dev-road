package com.derbyware.devroad.repositories;

import com.derbyware.devroad.entities.Version;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VersionRepository extends JpaRepository<Version, Long> {

	List<Version> findByTechnology_Id(long technologyId, Pageable pageable);
}
