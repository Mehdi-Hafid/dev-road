package com.derbyware.devroad.repositories;

import com.derbyware.devroad.entities.KeyHolder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KeyHolderRepository extends JpaRepository<KeyHolder, String> {

}
