package com.derbyware.devroad.entities.constructor;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.YearMonth;

@Data
@AllArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class DisplayUpgradeChild {

	private long id;

	private YearMonth acquired;

	private String name;

	private long versionId;

	private String versionName;

	private YearMonth versionReleaseDate;

}
