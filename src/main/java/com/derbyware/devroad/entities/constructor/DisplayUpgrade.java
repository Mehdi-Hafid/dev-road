package com.derbyware.devroad.entities.constructor;

import com.derbyware.devroad.dto.DisplayUpgradeChildDTO;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@RequiredArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "technologyId")
public class DisplayUpgrade {

	private final long technologyId;

	private final String technologyName;

	private final String seoName;

	private final String technologyImageLink;

	private List<DisplayUpgradeChildDTO> upgrades = new ArrayList<>();


}
