package com.derbyware.devroad.entities;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.YearMonth;

@Data
@Entity
public class Version {
	@Id
	@TableGenerator(name = "VERSION_GEN", table = "ID_GEN", pkColumnName = "GEN_TABLE_NAME",
			valueColumnName = "GEN_TABLE_VAL", pkColumnValue = "VERSION_Gen", initialValue = 1,
			allocationSize = 100)
	@GeneratedValue(generator = "VERSION_GEN")
	private long id;

	@ManyToOne
	@JoinColumn(name = "TECHNOLOGY_ID", referencedColumnName = "ID")
	private Technology technology;

	private String name;

	private YearMonth releaseDate;
	// added by user

	private LocalDateTime createdAt;

	@Lob
	private String description;
}
