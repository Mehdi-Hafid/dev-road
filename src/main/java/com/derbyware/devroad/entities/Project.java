package com.derbyware.devroad.entities;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@ToString(exclude = {"upgrades"})
public class Project {
	@Id
	@TableGenerator(name = "PROJECT_GEN", table = "ID_GEN", pkColumnName = "GEN_TABLE_NAME",
			valueColumnName = "GEN_TABLE_VAL", pkColumnValue = "PROJECT_Gen", initialValue = 1,
			allocationSize = 100)
	@GeneratedValue(generator = "PROJECT_GEN")
	private long id;
	//user

	private String title;

	private YearMonth fromDate;

	private YearMonth toDate;

	@Lob
	private String detail;

	@ManyToMany(mappedBy = "projects")
	private List<Upgrade> upgrades = new ArrayList<>();

	private LocalDateTime createdAt;

}
