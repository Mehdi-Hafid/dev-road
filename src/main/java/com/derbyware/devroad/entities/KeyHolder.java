package com.derbyware.devroad.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class KeyHolder {

	@Id
	private String encoded;
}
