package com.derbyware.devroad.entities;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Upgrade {
	@Id
	@TableGenerator(name = "UPGRADE_GEN", table = "ID_GEN", pkColumnName = "GEN_TABLE_NAME",
			valueColumnName = "GEN_TABLE_VAL", pkColumnValue = "UPGRADE_Gen", initialValue = 1,
			allocationSize = 100)
	@GeneratedValue(generator = "UPGRADE_GEN")
	private long id;
	//user

	//Unidirectional: Upgrade is related to user
	@OneToOne
	@JoinColumn(name = "VERSION_ID", referencedColumnName = "ID")
	private Version version;

	private YearMonth acquired;

	private String name;

	@Lob
	private String books;

	@Lob
	private String courses;

	@Lob
	private String achievements;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "UPGRADE_PROJECT", joinColumns = @JoinColumn(name = "UPGRADE_ID"),
			inverseJoinColumns = @JoinColumn(name = "PROJECT_ID"))
	private List<Project> projects = new ArrayList<>();

	private LocalDateTime createdAt;

	@Lob
	private String description;

}
