package com.derbyware.devroad.entities;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@ToString(exclude = {"versions", "children"})
@Entity
public class Technology {

	@Id
	@TableGenerator(name = "TECHNOLOGY_GEN", table = "ID_GEN", pkColumnName = "GEN_TABLE_NAME",
			valueColumnName = "GEN_TABLE_VAL", pkColumnValue = "TECHNOLOGY_Gen", initialValue = 1,
			allocationSize = 100)
	@GeneratedValue(generator = "TECHNOLOGY_GEN")
	private long id;

	private String name;

	//SEO Name, unique
	@Column(unique = true, name = "SEO_NAME")
	private String seoName;

	@OneToMany(mappedBy = "technology", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Version> versions = new ArrayList<>();

	private String imageLink;
	// added by user

	@ManyToOne
	@JoinColumn(name = "PARENT_ID", referencedColumnName = "ID")
	private Technology parent;

	@OneToMany(mappedBy = "parent")
	private List<Technology> children = new ArrayList<>();

	private LocalDateTime createdAt;

	@Lob
	private String description;
}
