module derbyware.devroad {
	requires spring.beans;
	requires spring.web;
	requires org.mapstruct;
	requires lombok;
	requires java.persistence;
	requires spring.context;
	requires spring.data.jpa;
	requires spring.boot.autoconfigure;
	requires spring.boot;
	requires java.sql;
	requires java.compiler;
	requires spring.security.config;
	requires spring.security.web;
	requires spring.data.commons;
	requires spring.webmvc;
	requires org.slf4j;
	requires java.validation;
	requires com.fasterxml.jackson.annotation;
	requires org.apache.commons.text;
	requires jjwt.api;
	requires spring.security.core;
	requires org.apache.tomcat.embed.core;
	requires com.fasterxml.jackson.databind;
}